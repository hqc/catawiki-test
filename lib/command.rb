require_relative 'state'

# Represents the command letter
class Command
  def initialize(state, command)
    @state = state
    @command = command
  end

  # Build the corresponding command object for commdn
  #
  # state - The current state to be change
  # command - Command to be invoked
  #
  # Returns DirectionCommand / MovementCommand
  def self.build(state, command)
    case command
    when *DirectionCommand::VALID_COMMANDS
      DirectionCommand.new(state, command)
    when MovementCommand::MOVE_COMMAND
      MovementCommand.new(state)
    else
      raise InvalidCommandError
    end
  end

  def call
    raise NotImplementedError
  end

  class InvalidCommandError < ::StandardError; end
end

require_relative 'commands/direction_command'
require_relative 'commands/movement_command'
