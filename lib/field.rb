# Represents the field
class Field
  attr_reader :x, :y

  def initialize(x:, y:)
    @x = x
    @y = y
  end

  def valid?(state)
    (0..@x).include?(state.x) && (0..@y).include?(state.y)
  end
end
