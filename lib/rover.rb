require_relative 'command'
require_relative 'state'

# Represents the rover
class Rover
  extend Forwardable
  def_delegators :@state, :x, :y, :direction

  attr_reader :field

  def initialize(x: 0, y: 0, direction:, field:)
    @state = State.new(x: x, y: y, direction: direction)
    @field = field
  end

  # Public: Executes a string of letters which moves the rover
  #
  # command_string - The string of letters to be executed
  #
  # Returns void
  def execute_cmd!(command_string)
    change_state(
      command_string.chars.inject(state) do |current_state, command|
        Command.build(current_state, command).()
      end
    )
  end

  def to_s
    state.to_s
  end

  private

  def change_state(state)
    @state = state if valid_state?(state)
  end

  # TODO: Validate if the current state is out of the field
  def valid_state?(state)
    field.valid?(state)
  end

  attr_reader :state
end
