# Represents the state of the rover
class State
  attr_reader :x, :y, :direction

  # List of directions of the rovers
  DIRECTIONS = %W{N E S W}.freeze

  def initialize(x:, y:, direction:)
    @x = x
    @y = y
    @direction = direction
  end

  # Rotate the direction clockwisely
  #
  # Returns the new direction
  def direction_rotate_clockwisely
    DIRECTIONS[(direction_index + 1) % 4]
  end

  # Rotate the direction counter-clockwisely
  #
  # Returns the new direction
  def direction_rotate_counterclockwisely
    DIRECTIONS[(direction_index - 1) % 4]
  end

  def to_s
    "#{x} #{y} #{direction}"
  end

  private

  def direction_index
    DIRECTIONS.index(direction)
  end
end

