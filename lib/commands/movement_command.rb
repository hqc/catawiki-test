class MovementCommand < ::Command

  # Move ahead command
  MOVE_COMMAND = 'M'.freeze

  def initialize(state)
    @state = state
  end

  # Execute the command itself and return the new state
  #
  # Returns State
  def call
    State.new(
      x: @state.x + delta[:dx],
      y: @state.y + delta[:dy],
      direction: @state.direction
    )
  end

  private

  def delta
    @delta ||= case @state.direction
               when 'N'
                 {dx: 0, dy: 1}
               when 'E'
                 {dx: 1, dy: 0}
               when 'S'
                 {dx: 0, dy: -1}
               when 'W'
                 {dx: -1, dy: 0}
               end
  end
end
