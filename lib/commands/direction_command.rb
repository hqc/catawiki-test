# Represents the direction command
class DirectionCommand < ::Command

  # Rotate left command letter
  LEFT_COMMAND = 'L'.freeze

  # Rotate right command letter
  RIGHT_COMMAND = 'R'.freeze

  # List of valid direction commands
  VALID_COMMANDS = [LEFT_COMMAND, RIGHT_COMMAND]

  # Execute the command itself and return the new state
  #
  # Return State
  def call
    direction =
      case @command
      when LEFT_COMMAND
        @state.direction_rotate_counterclockwisely
      when RIGHT_COMMAND
        @state.direction_rotate_clockwisely
      end

    State.new(
      x: @state.x,
      y: @state.y,
      direction: direction
    )
  end
end
