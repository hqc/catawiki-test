require_relative '../spec_helper'

describe Command do
  describe '.build' do
    it 'builds the corresponding command object' do
      state = State.new(x: 1, y: 1, direction: 'N')
      command = Command.build(state, 'L')
      expect(command).to be_a(DirectionCommand)

      command = Command.build(state, 'R')
      expect(command).to be_a(DirectionCommand)

      command = Command.build(state, 'M')
      expect(command).to be_a(MovementCommand)

      expect do
        Command.build(state, 'A')
      end.to raise_error(Command::InvalidCommandError)
    end
  end

  describe '#call' do
    it 'sets the interface up' do
      state = State.new(x: 1, y: 1, direction: 'N')
      command = Command.new(state, 'L')
      expect { command.() }.to raise_error(NotImplementedError)
    end
  end
end
