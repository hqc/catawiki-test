require_relative '../spec_helper'

describe Rover do
  describe '#initialize' do
    it 'initializes a rover with coordinates x, y and direction, and field deploying to' do
      field = Field.new(x: 5, y: 5)
      rover = Rover.new(x: 0, y: 0, field: field, direction: 'N')

      expect(rover).to be_a(Rover)
      expect(rover.x).to eq 0
      expect(rover.y).to eq 0
      expect(rover.direction).to eq 'N'
      expect(rover.field).to eq field
    end
  end

  describe '#execute_cmd!' do
    it 'executes the command string and moves itself' do
      field = Field.new(x: 5, y: 5)
      rover = Rover.new(x: 3, y: 3, field: field, direction: 'N')

      rover.execute_cmd!('L')
      expect(rover.direction).to eq 'W'

      rover.execute_cmd!('R')
      expect(rover.direction).to eq 'N'

      rover.execute_cmd!('RR')
      expect(rover.direction).to eq 'S'

      rover.execute_cmd!('M')
      expect(rover.direction).to eq 'S'
      expect(rover.x).to eq 3
      expect(rover.y).to eq 2
    end
  end
end


