require_relative '../spec_helper'

describe Field do
  describe '#initialize' do
    it 'initializes the field with its upper-right coordinates' do
      field = Field.new(x: 5, y: 5)

      expect(field.x).to eq 5
      expect(field.y).to eq 5
    end
  end

  describe 'valid?' do
    it 'checks if state is valid on the field' do
      field = Field.new(x: 5, y: 5)

      state = State.new(x: 1, y: 0, direction: 'N')
      expect(field.valid?(state)).to be_truthy

      state = State.new(x: 6, y: 0, direction: 'N')
      expect(field.valid?(state)).to be_falsey
    end
  end
end
