require_relative '../../spec_helper'

describe MovementCommand do
  describe '#call' do
    it 'returns state with the new coordinates' do
      state = State.new(x: 1, y: 1, direction: 'N')

      new_state = MovementCommand.new(state).()
      expect(new_state.direction).to eq 'N'
      expect(new_state.x).to eq 1
      expect(new_state.y).to eq 2

      new_state = MovementCommand.new(new_state).()
      expect(new_state.direction).to eq 'N'
      expect(new_state.x).to eq 1
      expect(new_state.y).to eq 3
    end
  end
end
