require_relative '../../spec_helper'

describe DirectionCommand do
  describe '#call' do
    it 'returns state with the new direction' do
      state = State.new(x: 1, y: 1, direction: 'N')

      new_state = DirectionCommand.new(state, 'L').()
      expect(new_state.direction).to eq 'W'
    end
  end
end
