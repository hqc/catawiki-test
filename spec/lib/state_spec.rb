require_relative '../spec_helper'

describe State do
  describe '#initialize' do
    it 'initializes a new state' do
      state = State.new(x: 1, y: 2, direction: 'N')

      expect(state.x).to eq 1
      expect(state.y).to eq 2
      expect(state.direction).to eq 'N'
    end
  end

  describe '#direction_rotate_clockwisely' do
    it 'rotates the direction clockwisely' do
      state = State.new(x: 1, y: 2, direction: 'N')

      expect(state.direction_rotate_clockwisely).to eq 'E'
    end
  end

  describe '#direction_rotate_counterclockwisely' do
    it 'rotates the direction counter-clockwisely' do
      state = State.new(x: 1, y: 2, direction: 'N')

      expect(state.direction_rotate_counterclockwisely).to eq 'W'
    end
  end

  describe '#to_s' do
    it 'outputs the state info in readable way' do
      state = State.new(x: 1, y: 2, direction: 'N')

      expect(state.to_s).to eq '1 2 N'
    end
  end
end
