require_relative '../spec_helper'

describe 'Rovers deployed on Mars acceptance test' do
  it 'explores the plateau' do
    field = Field.new(x: 5, y: 5)
    rover1 = Rover.new(x: 1, y: 2, field: field, direction: 'N')
    rover2 = Rover.new(x: 3, y: 3, field: field, direction: 'E')

    rover1.execute_cmd!('LMLMLMLMM')
    rover2.execute_cmd!('MMRMMRMRRM')

    expect(rover1.direction).to eq 'N'
    expect(rover1.x).to eq 1
    expect(rover1.y).to eq 3
    expect(rover1.to_s).to eq '1 3 N'

    expect(rover2.direction).to eq 'E'
    expect(rover2.x).to eq 5
    expect(rover2.y).to eq 1

    expect(rover2.to_s).to eq '5 1 E'
  end
end
